package com.example.dentistry.configuration;

import com.example.dentistry.models.User;
import com.example.dentistry.models.roleEnum;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.management.relation.Role;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CustomUserDetails extends org.springframework.security.core.userdetails.User {

    private final User user;

    public CustomUserDetails(User user) {
        super(user.getLogin_user(), user.getPassword_user(), getAuthorities(user.getRoles()));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    // Метод для получения ролей пользователя
    public static List<GrantedAuthority> getAuthorities(Set<roleEnum> roles) {
        return roles.stream()
                .map(roleEnum -> new SimpleGrantedAuthority("ROLE_" + roleEnum.name()))
                .collect(Collectors.toList());
    }


    // Добавьте геттеры для дополнительных полей, таких как имя, фамилия, отчество и т. д.
    public String getFullName() {
        return user.getFullName();
    }
}

