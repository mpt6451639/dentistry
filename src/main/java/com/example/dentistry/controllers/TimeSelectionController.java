package com.example.dentistry.controllers;

import com.example.dentistry.models.Employee;
import com.example.dentistry.models.Schedule;
import com.example.dentistry.models.User;
import com.example.dentistry.repositories.ScheduleRepo;
import com.example.dentistry.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TimeSelectionController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ScheduleRepo scheduleRepo;

    @GetMapping("/selectTime")
    public String selectTime(Model model, @RequestParam("dayOfWeek") int dayOfWeek, @RequestParam("doctorId") Long doctorId) {
        User selectedDoctor = userRepo.findById(doctorId).orElse(null);

        if (selectedDoctor != null) {
            List<Schedule> schedules = scheduleRepo.findByDayOfWeekAndEmployeeId(dayOfWeek, selectedDoctor.getEmployee().getId_employee());

            if (!schedules.isEmpty()) {
                model.addAttribute("schedules", schedules);
                model.addAttribute("selectedDoctor", selectedDoctor);
                model.addAttribute("selectedDayOfWeek", dayOfWeek);
                return "selectTime";
            } else {
                // Обработка ошибки - расписание не найдено
                return "error"; // Замените это на страницу ошибки
            }
        } else {
            // Обработка ошибки - врач не найден
            return "error"; // Замените это на страницу ошибки
        }
    }

    @PostMapping("/confirmReception")
    public String confirmReception(@RequestParam("doctorId") Long doctorId,
                                   @RequestParam("selectedDayOfWeek") int selectedDayOfWeek,
                                   @RequestParam("selectedTime") String selectedTime,
                                   Model model) {
        // Добавьте здесь логику для подтверждения записи на приём
        // Например, сохранение записи в базе данных или другие действия

        // Возвращаем страницу подтверждения, например
        model.addAttribute("selectedTime", selectedTime);
        return "profile";
    }
}
