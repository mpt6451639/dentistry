package com.example.dentistry.controllers;

import com.example.dentistry.configuration.CustomUserDetails;
import com.example.dentistry.models.User;
import com.example.dentistry.models.roleEnum;
import com.example.dentistry.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class appointmentController {

    @Autowired
    private UserRepo userRepo;

    @GetMapping("appointment")
    public String index(Model model, @AuthenticationPrincipal CustomUserDetails userDetails) {
        if (userDetails != null) {
            String fullName = userDetails.getFullName();
            model.addAttribute("fullName", fullName);
        }

        roleEnum doctorRole = roleEnum.DOCTOR;
        List<User> doctorUsers = userRepo.findByRolesContaining(doctorRole);
        model.addAttribute("doctorUsers", doctorUsers);

        Iterable<User> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "appointment";
    }

    @GetMapping("addReception/{id_employee}")
    public String Reception(
            @PathVariable Long id_employee,
            Model model,
            @AuthenticationPrincipal CustomUserDetails userDetails
    ) {
        if (userDetails != null) {
            String fullName = userDetails.getFullName();
            model.addAttribute("fullName", fullName);
        }

        Optional<User> optionalDoctor = userRepo.findById(id_employee);

        if (optionalDoctor.isPresent()) {
            User selectedDoctor = optionalDoctor.get();

            List<Long> workingDayIds = selectedDoctor.getEmployee().getSchedule().stream()
                    .map(schedule -> schedule.getDay_week_id().getId_day_week())
                    .distinct()
                    .collect(Collectors.toList());

            // Получаем текущую дату
            LocalDate currentDate = LocalDate.now();

            // Получаем следующие две недели
            List<String> futureDates = new ArrayList<>();

            for (int i = 0; i < 14; i++) {
                LocalDate nextDate = currentDate.plusDays(i);

                // Проверяем, является ли следующая дата рабочим днём
                if (workingDayIds.contains((long) nextDate.getDayOfWeek().getValue())) {
                    // Форматируем дату в нужный формат и добавляем в список
                    String formattedDate = nextDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                    futureDates.add(formattedDate);
                }
            }

            model.addAttribute("workingDays", workingDayIds);
            model.addAttribute("futureDates", futureDates);
            model.addAttribute("selectedDoctor", selectedDoctor);
            model.addAttribute("selectedDoctorSchedule", selectedDoctor.getEmployee().getSchedule());
        } else {
            return "redirect:/appointment";
        }

        return "addReception";
    }
}
