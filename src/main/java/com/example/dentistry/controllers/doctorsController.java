package com.example.dentistry.controllers;

import com.example.dentistry.configuration.CustomUserDetails;
import com.example.dentistry.models.User;
import com.example.dentistry.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/doctors")
public class doctorsController {

    private final UserRepo userRepo;

    @Autowired
    public doctorsController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping
    public String index(Model model, @AuthenticationPrincipal CustomUserDetails userDetails) {
        if (userDetails != null) {
            // Получайте нужные вам данные из userDetails
            String fullName = userDetails.getFullName();
            model.addAttribute("fullName", fullName);
        }

        Iterable<User> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "doctors";
    }
}
