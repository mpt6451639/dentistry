package com.example.dentistry.controllers;

import com.example.dentistry.configuration.CustomUserDetails;
import com.example.dentistry.models.User;
import com.example.dentistry.repositories.UserRepo;
import com.github.GBSEcom.client.auth.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/index")
public class indexController {

    private final UserRepo userRepo;

    @Autowired
    public indexController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping
    public String index(Model model, @AuthenticationPrincipal CustomUserDetails userDetails) {
        if (userDetails != null) {
            // Получайте нужные вам данные из userDetails
            String fullName = userDetails.getFullName();
            model.addAttribute("fullName", fullName);
        }

        Iterable<User> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "index";
    }


}

