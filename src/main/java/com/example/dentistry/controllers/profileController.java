package com.example.dentistry.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class profileController {

    @GetMapping("/profile")
    public String home() {
        return "profile";
    }
}
