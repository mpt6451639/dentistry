package com.example.dentistry.controllers;

import com.example.dentistry.models.Gender;
import com.example.dentistry.models.Patient;
import com.example.dentistry.models.User;
import com.example.dentistry.models.roleEnum;
import com.example.dentistry.repositories.GenderRepo;
import com.example.dentistry.repositories.PatientRepo;
import com.example.dentistry.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Collections;

@Controller
public class registerationController {

    @Autowired
    private UserRepo userRepository;


    @Autowired
    private PatientRepo patientRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private GenderRepo genderRepo;

    @GetMapping("/authorization")
    private String authorization() {
        return "authorization";
    }

    @GetMapping("/registration")
    private String regView(Model model) {
        User user = new User();
        Iterable<Gender> genders = genderRepo.findAll();
        model.addAttribute("genders", genders);
        model.addAttribute("user", user);
        return "registration";
    }

    @PostMapping("/registration")
    private String reg(
            @Valid @ModelAttribute("users") User user,
            @RequestParam(name = "id_gender") Long id_gender,
            Model model,
            BindingResult bindingResult
    ) {
        User userFromDb = userRepository.findByLogin_user(user.getLogin_user());

        if (userFromDb != null) {
            model.addAttribute("message", "Пользователь уже существует");

            return "registration";
        }

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        Iterable<Gender> genders = genderRepo.findAll();
        model.addAttribute("genders", genders);
        model.addAttribute("user", user);

        // Создаем объект Patient
        Patient patient = new Patient();
        patient.setUser(user);

        // Получаем выбранный пол из репозитория
        Gender gender = (id_gender != null) ? genderRepo.findById(id_gender).orElse(null) : null;

        // Устанавливаем пол пациента
        patient.setGender_id(gender);


        // Устанавливаем роль и шифруем пароль
        user.setRoles(Collections.singleton(roleEnum.PATIENT));
        user.setPassword_user(passwordEncoder.encode(user.getPassword_user()));

        //userRepository.save(user);
        patientRepo.save(patient);

        return "redirect:/authorization";
    }
}
