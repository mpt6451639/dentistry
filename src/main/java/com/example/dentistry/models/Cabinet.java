package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "cabinet")
public class Cabinet {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private long id_cabinet;

    @Min(value = 3, message = "Номер кабинета должен быть трёхзначным числом")
    @Max(value = 3, message = "Номер кабинета должен быть трёхзначным числом")
    @Column(name = "number_cabinet")
    private int number_cabinet;

    public Cabinet(){}

    public Cabinet(long id_cabinet, int number_cabinet) {
        this.id_cabinet = id_cabinet;
        this.number_cabinet = number_cabinet;
    }

    public long getId_cabinet() {
        return id_cabinet;
    }

    public void setId_cabinet(long id_cabinet) {
        this.id_cabinet = id_cabinet;
    }

    public int getNumber_cabinet() {
        return number_cabinet;
    }

    public void setNumber_cabinet(int number_cabinet) {
        this.number_cabinet = number_cabinet;
    }
}
