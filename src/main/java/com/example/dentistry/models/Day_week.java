package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Table(name = "day_week")
public class Day_week {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id_day_week;

        @NotBlank(message = "День недели не должна быть пустой")
        @Size(max = 11, message = "Длина дня недели должна быть не больше 11 символов")
        @Column(name = "name_day_week")
        private String name_day_week;

        public Day_week(){}

        public Day_week(long id_day_week, String name_day_week) {
                this.id_day_week = id_day_week;
                this.name_day_week = name_day_week;
        }

        public long getId_day_week() {
                return id_day_week;
        }

        public void setId_day_week(long id_day_week) {
                this.id_day_week = id_day_week;
        }

        public String getName_day_week() {
                return name_day_week;
        }

        public void setName_day_week(String name_day_week) {
                this.name_day_week = name_day_week;
        }
}
