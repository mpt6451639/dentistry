package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "diagnosis")
public class Diagnosis {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id_diagnosis;

        @NotBlank(message = "Диагноз не должна быть пустой")
        @Size(max = 50, message = "Длина диагноза должна быть не больше 50 символов")
        @Column(name = "name_diagnosis")
        private String name_diagnosis;

        public Diagnosis(){}

        public Diagnosis(long id_diagnosis, String name_diagnosis) {
                this.id_diagnosis = id_diagnosis;
                this.name_diagnosis = name_diagnosis;
        }

        public long getId_diagnosis() {
                return id_diagnosis;
        }

        public void setId_diagnosis(long id_diagnosis) {
                this.id_diagnosis = id_diagnosis;
        }

        public String getName_diagnosis() {
                return name_diagnosis;
        }

        public void setName_diagnosis(String name_diagnosis) {
                this.name_diagnosis = name_diagnosis;
        }
}
