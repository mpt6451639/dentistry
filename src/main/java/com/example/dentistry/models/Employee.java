package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Collection;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @JoinColumn(name = "id_employee")
    private Long id_employee;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id_employee")
    private User user;

    @NotBlank(message = "Дата не должна быть пустой")
    @Column(name = "date_work_employee")
    private LocalDate date_work_employee;

    @ManyToOne
    @JoinColumn(name = "position_id") // Имя столбца для внешнего ключа в таблице Employee
    private Position position_id;

    @ManyToOne
    @JoinColumn(name = "cabinet_id") // Имя столбца для внешнего ключа в таблице Employee
    private Cabinet cabinet_id;

    @OneToMany(mappedBy = "employee_id", fetch = FetchType.EAGER)
    private Collection<Schedule> schedule;

    public Employee(){}

    public Employee(User user, LocalDate date_work_employee, Position position_id, Cabinet cabinet_id) {
        this.id_employee = user.getId_user();
        this.user = user;
        this.date_work_employee = date_work_employee;
        this.position_id = position_id;
        this.cabinet_id = cabinet_id;
    }

    public Long getId_employee() {
        return id_employee;
    }

    public void setId_employee(Long id_employee) {
        this.id_employee = id_employee;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate_work_employee() {
        return date_work_employee;
    }

    public void setDate_work_employee(LocalDate date_work_employee) {
        this.date_work_employee = date_work_employee;
    }

    public Position getPosition_id() {
        return position_id;
    }

    public void setPosition_id(Position position_id) {
        this.position_id = position_id;
    }

    public Cabinet getCabinet_id() {
        return cabinet_id;
    }

    public void setCabinet_id(Cabinet cabinet_id) {
        this.cabinet_id = cabinet_id;
    }

    public Collection<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(Collection<Schedule> schedule) {
        this.schedule = schedule;
    }
}
