package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "gender")
public class Gender {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id_gender;

        @NotBlank(message = "Пол не должен быть пустой")
        @Size(max = 7, message = "Длина пола должна быть не больше 7 символов")
        @Column(name = "name_gender")
        private String name_gender;

        public Gender(){}

        public Gender(long id_gender, String name_gender) {
                this.id_gender = id_gender;
                this.name_gender = name_gender;
        }

        public long getId_gender() {
                return id_gender;
        }

        public void setId_gender(long id_gender) {
                this.id_gender = id_gender;
        }

        public String getName_gender() {
                return name_gender;
        }

        public void setName_gender(String name_gender) {
                this.name_gender = name_gender;
        }
}
