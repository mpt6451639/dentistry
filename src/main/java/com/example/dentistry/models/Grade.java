package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "grade")
public class Grade {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id_grade;

        @Min(value = 1, message = "Оценка должен быть однозначным или двухзначным числом")
        @Max(value = 2, message = "Оценка должен быть однозначным или двухзначным числом")
        @Column(name = "name_grade")
        private int name_grade;

        public Grade(){}

        public Grade(long id_grade, int name_grade) {
                this.id_grade = id_grade;
                this.name_grade = name_grade;
        }

        public long getId_grade() {
                return id_grade;
        }

        public void setId_grade(long id_grade) {
                this.id_grade = id_grade;
        }

        public int getName_grade() {
                return name_grade;
        }

        public void setName_grade(int name_grade) {
                this.name_grade = name_grade;
        }
}
