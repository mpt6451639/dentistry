package com.example.dentistry.models;

import javax.persistence.*;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @JoinColumn(name = "id_patient")
    private Long id_patient;

    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "id_patient")
    private User user;

    @ManyToOne
    @JoinColumn(name = "gender_id") // Имя столбца для внешнего ключа в таблице Employee
    private Gender gender_id;

    public Patient() {}

    public Patient(User user, Gender gender) {
        this.user = user;
        this.gender_id = gender;
        // Не устанавливайте id_patient вручную
        // this.id_patient = user.getId_user();
    }

    public Long getId_patient() {
        return id_patient;
    }

    public void setId_patient(Long id_patient) {
        this.id_patient = id_patient;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Gender getGender_id() {
        return gender_id;
    }

    public void setGender_id(Gender gender_id) {
        this.gender_id = gender_id;
    }
}
