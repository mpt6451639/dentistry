package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "payment")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_payment;

    @Size(max = 100, message = "Длина симптомов должна быть не больше 100 символов")
    @Column(name = "sum_payment", columnDefinition = "numeric")
    private BigDecimal sum_payment;

    @NotBlank(message = "Дата не должна быть пустой")
    @Column(name = "date_payment")
    private LocalDate date_payment;

    @ManyToOne
    @JoinColumn(name = "reception_id") // Имя столбца для внешнего ключа в таблице Employee
    private Reception reception_id;

    public Payment(){}

    public Payment(long id_payment, BigDecimal sum_payment, LocalDate date_payment, Reception reception_id) {
        this.id_payment = id_payment;
        this.sum_payment = sum_payment;
        this.date_payment = date_payment;
        this.reception_id = reception_id;
    }

    public long getId_payment() {
        return id_payment;
    }

    public void setId_payment(long id_payment) {
        this.id_payment = id_payment;
    }

    public BigDecimal getSum_payment() {
        return sum_payment;
    }

    public void setSum_payment(BigDecimal sum_payment) {
        this.sum_payment = sum_payment;
    }

    public LocalDate getDate_payment() {
        return date_payment;
    }

    public void setDate_payment(LocalDate date_payment) {
        this.date_payment = date_payment;
    }

    public Reception getReception_id() {
        return reception_id;
    }

    public void setReception_id(Reception reception_id) {
        this.reception_id = reception_id;
    }
}
