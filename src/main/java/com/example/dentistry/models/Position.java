package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "position")
public class Position {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id_position;

        @NotBlank(message = "Должность не должна быть пустой")
        @Size(max = 50, message = "Длина должности должна быть не больше 50 символов")
        @Column(name = "name_position")
        private String name_position;

        public Position(){}

        public Position(long id_position, String name_position) {
                this.id_position = id_position;
                this.name_position = name_position;
        }

        public long getId_position() {
                return id_position;
        }

        public void setId_position(long id_position) {
                this.id_position = id_position;
        }

        public String getName_position() {
                return name_position;
        }

        public void setName_position(String name_position) {
                this.name_position = name_position;
        }
}
