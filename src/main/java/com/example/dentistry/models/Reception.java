package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "reception")
public class Reception {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_reception;

    @Size(max = 100, message = "Длина симптомов должна быть не больше 100 символов")
    @Column(name = "symptoms_reception")
    private String symptoms_reception;

    @Size(max = 200, message = "Длина заключения должна быть не больше 100 символов")
    @Column(name = "conclusion_reception")
    private String conclusion_reception;

    @NotBlank(message = "Дата не должна быть пустой")
    @Column(name = "date_reception")
    private LocalDate date_reception;

    @Column(name = "time_reception")
    private LocalTime time_reception;

    @ManyToOne
    @JoinColumn(name = "status_reception_id") // Имя столбца для внешнего ключа в таблице Employee
    private Status_reception status_reception_id;

    @ManyToOne
    @JoinColumn(name = "patient_id") // Имя столбца для внешнего ключа в таблице Employee
    private Patient patient_id;

    @ManyToOne
    @JoinColumn(name = "employee_id") // Имя столбца для внешнего ключа в таблице Employee
    private Employee employee_id;

    @ManyToOne
    @JoinColumn(name = "diagnosis_id") // Имя столбца для внешнего ключа в таблице Employee
    private Diagnosis diagnosis_id;

    public Reception(){}


}
