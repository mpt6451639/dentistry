package com.example.dentistry.models;

import javax.persistence.*;

@Entity
@Table(name = "reception_service")
public class Reception_service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_reception_service;

    @ManyToOne
    @JoinColumn(name = "reception_id") // Имя столбца для внешнего ключа в таблице Employee
    private Reception reception_id;

    @ManyToOne
    @JoinColumn(name = "service_id") // Имя столбца для внешнего ключа в таблице Employee
    private Service service_id;

    public Reception_service(){}

    public Reception_service(long id_reception_service, Reception reception_id, Service service_id) {
        this.id_reception_service = id_reception_service;
        this.reception_id = reception_id;
        this.service_id = service_id;
    }

    public long getId_reception_service() {
        return id_reception_service;
    }

    public void setId_reception_service(long id_reception_service) {
        this.id_reception_service = id_reception_service;
    }

    public Reception getReception_id() {
        return reception_id;
    }

    public void setReception_id(Reception reception_id) {
        this.reception_id = reception_id;
    }

    public Service getService_id() {
        return service_id;
    }

    public void setService_id(Service service_id) {
        this.service_id = service_id;
    }
}
