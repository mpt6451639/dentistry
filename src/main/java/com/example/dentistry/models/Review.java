package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "review")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_review;

    @Column(name = "date_review")
    private LocalDate date_review;

    @NotBlank(message = "Текст отзыва не должен быть пустым")
    @Size(max = 200, message = "Длина текста отзыва должна быть 200 символов")
    @Column(name = "text_review")
    private String text_review;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient_id;

    @ManyToOne
    @JoinColumn(name = "grade_id")
    private Grade grade_id;


    public Review(){}

    public Review(long id_review, LocalDate date_review, String text_review, Patient patient_id, Grade grade_id) {
        this.id_review = id_review;
        this.date_review = date_review;
        this.text_review = text_review;
        this.patient_id = patient_id;
        this.grade_id = grade_id;
    }

    public long getId_review() {
        return id_review;
    }

    public void setId_review(long id_review) {
        this.id_review = id_review;
    }

    public LocalDate getDate_review() {
        return date_review;
    }

    public void setDate_review(LocalDate date_review) {
        this.date_review = date_review;
    }

    public String getText_review() {
        return text_review;
    }

    public void setText_review(String text_review) {
        this.text_review = text_review;
    }

    public Patient getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Patient patient_id) {
        this.patient_id = patient_id;
    }

    public Grade getGrade_id() {
        return grade_id;
    }

    public void setGrade_id(Grade grade_id) {
        this.grade_id = grade_id;
    }
}
