package com.example.dentistry.models;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "schedule")
@IdClass(ScheduleId.class)
public class Schedule {

    @Id
    @ManyToOne
    @JoinColumn(name = "day_week_id")
    private Day_week day_week_id;

    @Id
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee_id;

    @Column(name = "start_time")
    private LocalTime start_time;

    @Column(name = "end_time")
    private LocalTime end_time;

    public Schedule(){}

    public Schedule(Day_week day_week_id, Employee employee_id, LocalTime start_time, LocalTime end_time) {
        this.day_week_id = day_week_id;
        this.employee_id = employee_id;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public Day_week getDay_week_id() {
        return day_week_id;
    }

    public void setDay_week_id(Day_week day_week_id) {
        this.day_week_id = day_week_id;
    }

    public Employee getEmployee_id() {
        return employee_id;
    }


    public void setEmployee_id(Employee employee_id) {
        this.employee_id = employee_id;
    }

    public LocalTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalTime start_time) {
        this.start_time = start_time;
    }

    public LocalTime getEnd_time() {
        return end_time;
    }

    public void setEnd_time(LocalTime end_time) {
        this.end_time = end_time;
    }
}

