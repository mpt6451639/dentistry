package com.example.dentistry.models;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

public class ScheduleId implements Serializable {

    private Long day_week_id;
    private Long employee_id;

    public ScheduleId() {}

    public ScheduleId(Long day_week_id, Long employee_id) {
        this.day_week_id = day_week_id;
        this.employee_id = employee_id;
    }

    public Long getDay_week_id() {
        return day_week_id;
    }

    public void setDay_week_id(Long day_week_id) {
        this.day_week_id = day_week_id;
    }

    public Long getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Long employee_id) {
        this.employee_id = employee_id;
    }

    // Геттеры и сеттеры

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduleId that = (ScheduleId) o;
        return day_week_id.equals(that.day_week_id) &&
                employee_id.equals(that.employee_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(day_week_id, employee_id);
    }
}
