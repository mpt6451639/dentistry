package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "service")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_service;

    @Column(name = "price_service")
    private BigDecimal price_service;

    @NotBlank(message = "Название услугу не должен быть пустой")
    @Size(max = 50, message = "Длина названия услуги должна быть не больше 50 символов")
    @Column(name = "name_service")
    private String name_service;

    public Service(){}

    public Service(long id_service, BigDecimal price_service, String name_service) {
        this.id_service = id_service;
        this.price_service = price_service;
        this.name_service = name_service;
    }

    public long getId_service() {
        return id_service;
    }

    public void setId_service(long id_service) {
        this.id_service = id_service;
    }

    public BigDecimal getPrice_service() {
        return price_service;
    }

    public void setPrice_service(BigDecimal price_service) {
        this.price_service = price_service;
    }

    public String getName_service() {
        return name_service;
    }

    public void setName_service(String name_service) {
        this.name_service = name_service;
    }
}
