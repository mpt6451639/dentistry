package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "status_reception")
public class Status_reception {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id_status_reception;

    @NotBlank(message = "Статус приёма не должен быть пустой")
    @Size(max = 20, message = "Длина статуса приёма должна быть не больше 20 символов")
    @Column(name = "name_status_reception")
    private String name_status_reception;

    public Status_reception(){}

    public Status_reception(long id_status_reception, String name_status_reception) {
        this.id_status_reception = id_status_reception;
        this.name_status_reception = name_status_reception;
    }

    public long getId_status_reception() {
        return id_status_reception;
    }

    public void setId_status_reception(long id_status_reception) {
        this.id_status_reception = id_status_reception;
    }

    public String getName_status_reception() {
        return name_status_reception;
    }

    public void setName_status_reception(String name_status_reception) {
        this.name_status_reception = name_status_reception;
    }
}
