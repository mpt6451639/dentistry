package com.example.dentistry.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id_user;

    @NotBlank(message = "Логин не должен быть пустой")
    @Size(max = 36, message = "Длина логина должна быть не больше 36 символов")
    @Column(name = "username")
    private String login_user;

    @NotBlank(message = "Пароль не должен быть пустой")
    @Size(max = 255, message = "Длина пароля должна быть не больше 36 символов")
    @Column(name = "password")
    private String password_user;

    @NotBlank(message = "Фамилия не должна быть пустой")
    @Size(max = 36, message = "Длина фамилии должна быть не больше 36 символов")
    @Column(name = "surname_user")
    private String surname_user;

    @NotBlank(message = "Имя не должно быть пустым")
    @Size(max = 36, message = "Длина имени должна быть не больше 36 символов")
    @Column(name = "name_user")
    private String name_user;

    @Size(max = 36, message = "Длина отчества должна быть не больше 36 символов")
    @Column(name = "patronymic_user")
    private String patronymic_user;

    @NotBlank(message = "Номер телефона не должен быть пустым")
    @Size(max = 16, message = "Длина телефона должна быть 16 символов")
    @Column(name = "phone_user")
    private String phone_user;

    @NotBlank(message = "Почта не должна быть пустой")
    @Size(max = 50, message = "Почта должна быть не больше 50 символов")
    @Column(name = "email_user")
    private String email_user;

    @ElementCollection(targetClass = roleEnum.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<roleEnum> roles;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Patient patient;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Employee employee;

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Set<roleEnum> getRoles() {
        return roles;
    }

    public void setRoles(Set<roleEnum> roles) {
        this.roles = roles;
    }

    public User(){}

    public User(String login_user, String password_user, String surname_user, String name_user, String patronymic_user, String phone_user, String email_user, Set<roleEnum> roles, Patient patient) {
        this.login_user = login_user;
        this.password_user = password_user;
        this.surname_user = surname_user;
        this.name_user = name_user;
        this.patronymic_user = patronymic_user;
        this.phone_user = phone_user;
        this.email_user = email_user;
        this.roles = roles;
        this.patient = patient;
    }

    public Long getId_user() {
        return id_user;
    }
    public void setId(Long id_user) {
        this.id_user = id_user;
    }

    public String getLogin_user() {
        return login_user;
    }

    public void setLogin_user(String login_user) {
        this.login_user = login_user;
    }

    public String getPassword_user() {
        return password_user;
    }

    public void setPassword_user(String password_user) {
        this.password_user = password_user;
    }

    public String getSurname_user() {
        return surname_user;
    }

    public void setSurname_user(String surname_user) {
        this.surname_user = surname_user;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getPatronymic_user() {
        return patronymic_user;
    }

    public void setPatronymic_user(String patronymic_user) {
        this.patronymic_user = patronymic_user;
    }

    public String getPhone_user() {
        return phone_user;
    }

    public void setPhone_user(String phone_user) {
        this.phone_user = phone_user;
    }

    public String getEmail_user() {
        return email_user;
    }

    public void setEmail_user(String email_user) {
        this.email_user = email_user;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getFullName() {
        return surname_user + " " + name_user  + " " + patronymic_user;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}

