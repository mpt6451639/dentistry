package com.example.dentistry.repositories;

import com.example.dentistry.models.Cabinet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CabinetRepo extends JpaRepository<Cabinet, Long> {
    //Cabinet findByName(String name);
}
