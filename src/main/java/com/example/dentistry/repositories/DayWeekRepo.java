package com.example.dentistry.repositories;

import com.example.dentistry.models.Day_week;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DayWeekRepo extends JpaRepository<Day_week, Long> {
    //Day_week findByName(String name);
}
