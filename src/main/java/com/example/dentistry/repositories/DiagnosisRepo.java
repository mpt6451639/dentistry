package com.example.dentistry.repositories;

import com.example.dentistry.models.Diagnosis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiagnosisRepo extends JpaRepository<Diagnosis, Long> {
    //Diagnosis findByName(String name);
}
