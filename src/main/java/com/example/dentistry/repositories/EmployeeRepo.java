package com.example.dentistry.repositories;

import com.example.dentistry.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    //Employee findByName(String name);
}
