package com.example.dentistry.repositories;

import com.example.dentistry.models.Gender;
import com.example.dentistry.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface GenderRepo extends JpaRepository<Gender, Long> {
    @Query("SELECT g FROM Gender g WHERE g.name_gender = :name_gender")
    Gender findByName_gender(@Param("name_gender") String name_gender);
}

