package com.example.dentistry.repositories;

import com.example.dentistry.models.Grade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GradeRepo extends JpaRepository<Grade, Long> {
    //Grade findByName(String name);
}
