package com.example.dentistry.repositories;

import com.example.dentistry.models.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepo extends JpaRepository<Patient, Long> {
    //Patient findByName(String name);
    Patient save(Patient patient);
}
