package com.example.dentistry.repositories;

import com.example.dentistry.models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepo extends JpaRepository<Payment, Long> {
   //Payment findByName(String name);
}
