package com.example.dentistry.repositories;

import com.example.dentistry.models.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepo extends JpaRepository<Position, Long> {
  //  Position findByName(String name);
}
