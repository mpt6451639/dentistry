package com.example.dentistry.repositories;

import com.example.dentistry.models.Reception;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceptionRepo extends JpaRepository<Reception, Long> {
    //Reception findByName(String name);
}
