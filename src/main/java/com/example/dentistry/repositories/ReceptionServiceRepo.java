package com.example.dentistry.repositories;

import com.example.dentistry.models.Reception_service;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceptionServiceRepo extends JpaRepository<Reception_service, Long> {
    //Reception_service findByName(String name);
}
