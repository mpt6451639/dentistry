package com.example.dentistry.repositories;

import com.example.dentistry.models.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepo extends JpaRepository<Review, Long> {
    //Review findByName(String name);
}
