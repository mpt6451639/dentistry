package com.example.dentistry.repositories;

import com.example.dentistry.models.Employee;
import com.example.dentistry.models.Schedule;
import com.example.dentistry.models.ScheduleId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ScheduleRepo extends JpaRepository<Schedule, ScheduleId> {
    Optional<Schedule> findById(ScheduleId id);

    void deleteById(ScheduleId id);

    // Добавленный метод для поиска расписания врача по дню недели и идентификатору врача
    List<Schedule> findByDayOfWeekAndEmployeeId(int dayOfWeek, Long employeeId);
}
