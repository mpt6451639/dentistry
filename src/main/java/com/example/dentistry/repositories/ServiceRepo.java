package com.example.dentistry.repositories;

import com.example.dentistry.models.Service;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepo extends JpaRepository<Service, Long> {
   // Service findByName(String name);
}
