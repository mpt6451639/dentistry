package com.example.dentistry.repositories;

import com.example.dentistry.models.Status_reception;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusReceptionRepo extends JpaRepository<Status_reception, Long> {
   // Status_reception findByName(String name);
}
