package com.example.dentistry.repositories;

import com.example.dentistry.models.User;
import com.example.dentistry.models.roleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

public interface UserRepo extends JpaRepository<User, Long> {
   @Query("SELECT u FROM User u WHERE u.login_user = :username")
   User findByLogin_user(@Param("username") String username);

   @Transactional
   @Modifying
   @Query("UPDATE User u SET u.roles = ?2 WHERE u.id_user = ?1")
   void updateUserRoleById(Long id_user, String login_user, String password_user, Set<roleEnum> newRoles);

   @Query("SELECT DISTINCT u FROM User u JOIN u.roles r WHERE r NOT IN :roles")
   List<User> findByRolesNotIn(@Param("roles") Set<roleEnum> roles);


   List<User> findByRolesContaining(roleEnum role);
}



